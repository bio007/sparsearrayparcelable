package com.sygic.sparsearrayparcelableexample;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;


public class SparseArrayParcelableTestClass implements Parcelable {

    SparseArray<ParcelableTestClass> array;

    SparseArrayParcelableTestClass() {
        array = new SparseArray<>();
    }

    SparseArrayParcelableTestClass(Parcel in) {
        array = in.readSparseArray(ParcelableTestClass.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //dest.writeSparseArray(array); //FIXME: this should work out-of-the box but won't compile now
        writeSparseArrayWorkaround(dest, array);    //this works OK (check the test)
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SparseArrayParcelableTestClass> CREATOR = new Creator<SparseArrayParcelableTestClass>() {
        @Override
        public SparseArrayParcelableTestClass createFromParcel(Parcel in) {
            return new SparseArrayParcelableTestClass(in);
        }

        @Override
        public SparseArrayParcelableTestClass[] newArray(int size) {
            return new SparseArrayParcelableTestClass[size];
        }
    };

    private static void writeSparseArrayWorkaround(final Parcel parcel, final SparseArray<? extends Parcelable> array) {
        final SparseArray<Object> objectArray = new SparseArray<>(array.size());
        for (int i=0; i<array.size(); i++) {
            objectArray.put(array.keyAt(i), array.valueAt(i));
        }

        parcel.writeSparseArray(objectArray);
    }
}
