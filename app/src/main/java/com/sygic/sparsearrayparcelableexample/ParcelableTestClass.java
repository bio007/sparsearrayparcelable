package com.sygic.sparsearrayparcelableexample;

import android.os.Parcel;
import android.os.Parcelable;


public class ParcelableTestClass implements Parcelable {

    int no;

    ParcelableTestClass(int no) {
        this.no = no;
    }

    private ParcelableTestClass(Parcel in) {
        no = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(no);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParcelableTestClass that = (ParcelableTestClass) o;

        return no == that.no;
    }

    @Override
    public int hashCode() {
        return no;
    }

    public static final Creator<ParcelableTestClass> CREATOR = new Creator<ParcelableTestClass>() {
        @Override
        public ParcelableTestClass createFromParcel(Parcel in) {
            return new ParcelableTestClass(in);
        }

        @Override
        public ParcelableTestClass[] newArray(int size) {
            return new ParcelableTestClass[size];
        }
    };
}
