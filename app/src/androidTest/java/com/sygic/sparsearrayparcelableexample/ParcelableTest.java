package com.sygic.sparsearrayparcelableexample;

import android.os.Parcel;
import android.util.SparseArray;
import androidx.test.runner.AndroidJUnit4;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4.class)
public class ParcelableTest {

    private static final int KEY = 5;

    @Test
    public void parcelTest() {

        final ParcelableTestClass testClass = new ParcelableTestClass(8);

        final SparseArrayParcelableTestClass o1 = new SparseArrayParcelableTestClass();
        o1.array.put(KEY, testClass);

        final Parcel p1 = Parcel.obtain();
        final Parcel p2 = Parcel.obtain();

        o1.writeToParcel(p1, 0);
        final byte[] bytes = p1.marshall();

        p2.unmarshall(bytes, 0, bytes.length);
        p2.setDataPosition(0);

        final SparseArrayParcelableTestClass o2 = new SparseArrayParcelableTestClass(p2);

        Assert.assertEquals(o1.array.keyAt(0), 5);
        Assert.assertEquals(o2.array.keyAt(0), 5);

        Assert.assertEquals(o1.array.get(KEY).no, 8);
        Assert.assertEquals(o2.array.get(KEY).no, 8);

        assertEqualsSparseArray(o1.array, o2.array);
    }

    private static void assertEqualsSparseArray(final SparseArray<?> first, final SparseArray<?> second) {
        Assert.assertEquals(first.size(), second.size());

        for (int i = 0; i < first.size(); i++) {
            Assert.assertEquals(first.get(first.keyAt(i)), second.get(second.keyAt(i)));
        }
    }
}
